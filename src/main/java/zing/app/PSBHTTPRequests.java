package zing.app;

import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zing.models.psb.input.InputRequest;
import zing.models.psb.qr.QRCodePSB;
import zing.models.psb.qr.QRRequest;
import zing.models.psb.qr.QRResponse;
import zing.models.psb.qr.QRStatus;

import java.util.concurrent.CompletableFuture;

@Component
@Async("threadPoolTaskExecutor")
public class PSBHTTPRequests {
    private final RestTemplate restTemplate;
    private final Gson gson = new Gson();

    public PSBHTTPRequests(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CompletableFuture<QRResponse> getTerminalInfo(InputRequest requestTerminal, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = requestTerminal.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), QRResponse.class));
    }

    public CompletableFuture<QRCodePSB> genQR(QRRequest requestQR, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = requestQR.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), QRCodePSB.class));
    }

    public CompletableFuture<QRStatus> getQRStatus(String QRId, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map;
        map = new LinkedMultiValueMap<>();
        map.add("QR_ID", QRId);


        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), QRStatus.class));
    }
}
