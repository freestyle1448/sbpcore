package zing.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import zing.models.B07.B07;
import zing.models.B08.B08;
import zing.models.B23.B23;
import zing.models.B24.B24;
import zing.services.SBPService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.Callable;

@RestController
public class SBPController {
    private final SBPService sbpService;

    public SBPController(SBPService sbpService) {
        this.sbpService = sbpService;
    }

    @PostMapping("/ConfirmationRequestFromIPS")
    public Callable<ResponseEntity<B08>> confirmationRequestFromIPS(HttpEntity<String> entity) throws JAXBException
            , UnsupportedEncodingException {
        JAXBContext jaxbContext = JAXBContext.newInstance(B07.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(URLDecoder.decode(Objects.requireNonNull(entity.getBody())
                , StandardCharsets.UTF_8.toString()));
        zing.models.B07.B07 b07 = (B07) jaxbUnmarshaller.unmarshal(reader);

        return () -> ResponseEntity.ok(sbpService.confirmationRequestFromIPS(b07).get());
    }

    @PostMapping("/AcknowlegementToBenificiaryBank")
    public Callable<ResponseEntity<B24>> acknowlegementToBenificiaryBank(HttpEntity<String> entity) throws JAXBException
            , UnsupportedEncodingException {
        JAXBContext jaxbContext = JAXBContext.newInstance(B23.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(URLDecoder.decode(Objects.requireNonNull(entity.getBody())
                , StandardCharsets.UTF_8.toString()));
        zing.models.B23.B23 b23 = (B23) jaxbUnmarshaller.unmarshal(reader);

        return () -> ResponseEntity.ok(sbpService.acknowlegementToBenificiaryBank(b23).get());
    }
}
