package zing.controllers;

import org.springframework.web.bind.annotation.RestController;
import zing.models.IPSTransaction.IPSTransaction;
import zing.repositories.IPSTransactionsRepository;
import zing.repositories.QRCodesRepositories;

@RestController
public class Test {
    private final IPSTransactionsRepository ipsTransactionsRepository;
    private final QRCodesRepositories qrCodesRepositories;

    public Test(IPSTransactionsRepository ipsTransactionsRepository, QRCodesRepositories qrCodesRepositories) {
        this.ipsTransactionsRepository = ipsTransactionsRepository;
        this.qrCodesRepositories = qrCodesRepositories;
    }

    //@GetMapping("/")
    public void test() {
        ipsTransactionsRepository.insert(IPSTransaction.builder().build());
    }
}
