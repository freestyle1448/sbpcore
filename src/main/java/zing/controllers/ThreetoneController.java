package zing.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import zing.models.psb.qr.QRCodePSB;
import zing.models.psb.qr.QRStatus;
import zing.services.ThreetoneService;

import java.util.concurrent.Callable;

@RestController
public class ThreetoneController {
    private final ThreetoneService threetoneService;

    public ThreetoneController(ThreetoneService threetoneService) {
        this.threetoneService = threetoneService;
    }

    @GetMapping("/get_qr")
    public Callable<QRCodePSB> getQR(String amount, String purpose, String id) {
        return () -> threetoneService.genQR(amount, purpose, id).get();
    }

    @GetMapping("/get_qr_status")
    public Callable<QRStatus> getQRStatus(String qr_id) {
        return () -> threetoneService.getStatus(qr_id).get();
    }
}
