package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "accounts")
public class Account {
    @Id
    private Object id;
    private String name;
    private String identifier;
    private ObjectId merchant;
    private Boolean active;
    private Boolean createStaticQR_allowed;
    private Boolean createDynamicQR_allowed;
    private Commission inCommission;
    private Balance balance;
    private String note;
}
