package zing.models;

public final class AnswerCode {
    //Компоненты
    public static final String SBP = "I";
    public static final String RECEIVER = "B";
    public static final String SENDER = "P";
    //Группы
    public static final String SUCCESS = "00";
    public static final String TIMEOUT = "01";
    public static final String VALIDATION = "02";
    public static final String RESERVED = "03";
    public static final String SBP_CANCEL = "04";
    public static final String WORK_CANCEL = "05";
    public static final String CRYPTOGRAPH = "06";
    public static final String TECH_ERROR = "07";
    public static final String RESOLUTION = "10";
    public static final String CHECK_ERROR = "99";

    public static final String SUCCESS_MES = "Успешная операция СБП";
    public static final String TIMEOUT_MES = "Таймаут СБП";
    public static final String VALIDATION_MES = "Валидация";
    public static final String RESERVED_MES = "Зарезервировано";
    public static final String SBP_CANCEL_MES = "Отказ РС СБП";
    public static final String WORK_CANCEL_MES = "Отказ от обработки";
    public static final String CRYPTOGRAPH_MES = "Криптография";
    public static final String TECH_ERROR_MES = "Техническая ошибка";
    public static final String RESOLUTION_MES = "Урегулирование";
    public static final String CHECK_ERROR_MES = "Ошибка, требующая операционного вмешательства";
    //Смыслы
}
