package zing.models.B07;

import lombok.*;
import zing.models.PACSObject;

import javax.xml.bind.annotation.*;
import java.util.List;

import static zing.models.B07.MyNamespacePrefixMapper.*;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "IPSEnvelope", namespace = ROOT_URI_B07)
public class B07 extends PACSObject {
    @XmlElement(name = "AppHdr", namespace = HEADER_URI)
    private AppHdr header_AppHdr;
    @XmlElement(name = "Document", namespace = DOCUMENT_URI_B07)
    private Document document_Document;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AppHdr {
        @XmlElement(name = "Fr", namespace = HEADER_URI)
        private Fr fr;
        @XmlElement(name = "To", namespace = HEADER_URI)
        private To to;
        @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
        private String BizMsgIdr;
        @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
        private String MsgDefIdr;
        @XmlElement(name = "BizSvc", namespace = HEADER_URI)
        private String BizSvc;
        @XmlElement(name = "CreDt", namespace = HEADER_URI)
        private String CreDt;
        @XmlElement(name = "SgntSr", namespace = HEADER_URI)
        private Sgntr sgntr;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Fr {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class To {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIId {
            @XmlElement(name = "FinInstnId", namespace = HEADER_URI)
            private FinInstnId FinInstnId;

            @Data
            @AllArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            @NoArgsConstructor
            @XmlType(name="FinInstnId")
            public static class FinInstnId {
                @XmlElement(name = "Othr", namespace = HEADER_URI)
                private Othr Othr;


                @Data
                @AllArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                @NoArgsConstructor
                @XmlType(name="Othr")
                public static class Othr {
                    @XmlElement(name = "Id", namespace = HEADER_URI)
                    private String Id;
                }
            }
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Sgntr {
            @XmlElement(name = "Sign", namespace = SIGN_URI)
            private String sign;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    @NoArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Document {
        @XmlElement(name = "FIToFICstmrCdtTrf", namespace = DOCUMENT_URI_B07)
        private FIToFICstmrCdtTrf fIToFICstmrCdtTrf;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIToFICstmrCdtTrf {
            @XmlElement(name = "GrpHdr", namespace = DOCUMENT_URI_B07)
            private GrpHdr GrpHdr;
            @XmlElement(name = "CdtTrfTxInf", namespace = DOCUMENT_URI_B07)
            private CdtTrfTxInf CdtTrfTxInf;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class GrpHdr {
                @XmlElement(name = "MsgId", namespace = DOCUMENT_URI_B07)
                private String MsgId;
                @XmlElement(name = "CreDtTm", namespace = DOCUMENT_URI_B07)
                private String CreDtTm;
                @XmlElement(name = "NbOfTxs", namespace = DOCUMENT_URI_B07)
                private String NbOfTxs;
                @XmlElement(name = "SttlmInf", namespace = DOCUMENT_URI_B07)
                private SttlmInf SttlmInf;
                @XmlElement(name = "PmtTpInf", namespace = DOCUMENT_URI_B07)
                private PmtTpInf PmtTpInf;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class SttlmInf {
                    @XmlElement(name = "SttlmMtd", namespace = DOCUMENT_URI_B07)
                    private String SttlmMtd;
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class PmtTpInf {
                    @XmlElement(name = "LclInstrm", namespace = DOCUMENT_URI_B07)
                    private LclInstrm LclInstrm;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class LclInstrm {
                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B07)
                        private String Prtry;
                    }
                }
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class CdtTrfTxInf {
                @XmlElement(name = "PmtId", namespace = DOCUMENT_URI_B07)
                private PmtId PmtId;
                @XmlElement(name = "IntrBkSttlmAmt", namespace = DOCUMENT_URI_B07)
                private IntrBkSttlmAmt IntrBkSttlmAmt;
                @XmlElement(name = "AccptncDtTm", namespace = DOCUMENT_URI_B07)
                private String AccptncDtTm;
                @XmlElement(name = "ChrgBr", namespace = DOCUMENT_URI_B07)
                private String ChrgBr;
                @XmlElement(name = "Dbtr", namespace = DOCUMENT_URI_B07)
                private Dbtr Dbtr;
                @XmlElement(name = "DbtrAgt", namespace = DOCUMENT_URI_B07)
                private DbtrAgt DbtrAgt;
                @XmlElement(name = "CdtrAgt", namespace = DOCUMENT_URI_B07)
                private CdtrAgt CdtrAgt;
                @XmlElement(name = "Cdtr", namespace = DOCUMENT_URI_B07)
                private Cdtr Cdtr;
                @XmlElement(name = "CdtrAcct", namespace = DOCUMENT_URI_B07)
                private CdtrAcct CdtrAcct;
                @XmlElement(name = "RmtInf", namespace = DOCUMENT_URI_B07)
                private RmtInf RmtInf;
                @XmlElement(name = "SplmtryData", namespace = DOCUMENT_URI_B07)
                private SplmtryData SplmtryData;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class PmtId {
                    @XmlElement(name = "EndToEndId", namespace = DOCUMENT_URI_B07)
                    private String EndToEndId;
                    @XmlElement(name = "TxId", namespace = DOCUMENT_URI_B07)
                    private String TxId;
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class IntrBkSttlmAmt {
                    @XmlValue
                    private String IntrBkSttlmAmt;
                    @XmlAttribute(name = "Ccy")
                    private String Ccy;
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class Dbtr {
                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                    private Id Id;

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    @XmlType(name="Id2")
                    public static class Id {
                        @XmlElement(name = "PrvtId", namespace = DOCUMENT_URI_B07)
                        private PrvtId PrvtId;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class PrvtId {
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                            private Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name="Othr1")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                                private String Id;
                                @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B07)
                                private SchmeNm SchmeNm;

                                @Data
                                @AllArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @NoArgsConstructor
                                @XmlType(name="SchmeNm")
                                public static class SchmeNm {
                                    @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B07)
                                    private String Prtry;
                                }
                            }
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class DbtrAgt {
                    @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B07)
                    private FinInstnId FinInstnId;

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    @XmlType(name="FinInstnId1")
                    public static class FinInstnId {
                        @XmlElement(name = "ClrSysMmbId", namespace = DOCUMENT_URI_B07)
                        private ClrSysMmbId ClrSysMmbId;
                        @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                        private Othr Othr;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class ClrSysMmbId {
                            @XmlElement(name = "MmbId", namespace = DOCUMENT_URI_B07)
                            private String MmbId;
                        }

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="Othr2")
                        public static class Othr {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                            private String Id;
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class CdtrAgt {
                    @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B07)
                    private FinInstnId FinInstnId;

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    @XmlType(name="FinInstnId2")
                    public static class FinInstnId {
                        @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                        private Othr Othr;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="Othr3")
                        public static class Othr {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                            private String Id;
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class Cdtr {
                    @XmlElement(name = "Nm", namespace = DOCUMENT_URI_B07)
                    private String Nm;
                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                    private Id Id;

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    @XmlType(name="Id")
                    public static class Id {
                        @XmlElementWrapper(name = "OrgId", namespace = DOCUMENT_URI_B07)
                        @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                        private List<Othr> OrgId;
                        @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                        private Othr Othr;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="Othr4")
                        public static class Othr {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                            private String Id;
                            @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B07)
                            private SchmeNm SchmeNm;

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name="SchmeNm1")
                            public static class SchmeNm {
                                @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B07)
                                private String Prtry;
                            }
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class CdtrAcct {
                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                    private Id id;

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    @XmlType(name="Id1")
                    public static class Id {
                        @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B07)
                        private Othr Othr;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="Othr5")
                        public static class Othr {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B07)
                            private String Id;
                            @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B07)
                            private SchmeNm SchmeNm;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            public static class SchmeNm {
                                @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B07)
                                private String Prtry;
                            }
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class RmtInf {
                    @XmlElement(name = "Ustrd", namespace = DOCUMENT_URI_B07)
                    private String Ustrd;
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class SplmtryData {
                    @XmlElement(name = "Envlp", namespace = DOCUMENT_URI_B07)
                    private Envlp Envlp;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Envlp {
                        @XmlElement(name = "IpsDt", namespace = DOCUMENT_URI_B07)
                        private IpsDt IpsDt;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class IpsDt {
                            @XmlElement(name = "FrScr", namespace = DOCUMENT_URI_B07)
                            private String FrScr;
                            @XmlElement(name = "OperDate", namespace = DOCUMENT_URI_B07)
                            private String OperDate;
                        }
                    }
                }
            }
        }
    }
}
