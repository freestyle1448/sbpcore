package zing.models.B07;

import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;

public class MyNamespacePrefixMapper extends NamespacePrefixMapper {
    public static final String ROOT_URI_B07 = "ISO200022PayMessageRequest";
    public static final String ROOT_URI_B08 = "ISO200022PayMessageResponse";
    public static final String HEADER_URI = "urn:iso:std:iso:20022:tech:xsd:head.001.001.01";
    public static final String DOCUMENT_URI_B07 = "urn:iso:std:iso:20022:tech:xsd:pacs.008.001.07";
    public static final String DOCUMENT_URI_B08 = "urn:iso:std:iso:20022:tech:xsd:pacs.002.001.09";
    public static final String SIGN_URI = "http://www.w3.org/2000/09/xmldsig#";
    public static final String XSI_URI = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String SCHEMA_LOCATION_URI_B07 = "ISO200022PayMessageRequest PayMsgReq.xsd";
    public static final String SCHEMA_LOCATION_URI_B08 = "ISO200022PayMessageResponse PayMsgRes.xsd";

    public static final String ROOT_PREFIX = "";
    public static final String HEADER_PREFIX = "header";
    public static final String DOCUMENT_PREFIX = "document";
    public static final String SIGN_PREFIX = "sign";
    public static final String XSI_PREFIX = "xsi";


    @Override
    public String getPreferredPrefix(String s, String s1, boolean b) {
        switch (s) {
            case ROOT_URI_B07:
            case ROOT_URI_B08:
                return ROOT_PREFIX;
            case HEADER_URI:
                return HEADER_PREFIX;
            case DOCUMENT_URI_B07:
                return DOCUMENT_PREFIX;
            case SIGN_URI:
                return SIGN_PREFIX;
        }
        return s1;
    }
}
