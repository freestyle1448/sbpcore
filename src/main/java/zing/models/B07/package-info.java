@XmlSchema(
        namespace = ROOT_URI_B07,
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(namespaceURI = XSI_URI, prefix = XSI_PREFIX)
        }
)
package zing.models.B07;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

import static zing.models.B07.MyNamespacePrefixMapper.*;