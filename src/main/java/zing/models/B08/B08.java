package zing.models.B08;

import lombok.*;
import zing.models.PACSObject;

import javax.xml.bind.annotation.*;
import java.util.List;

import static zing.models.B07.MyNamespacePrefixMapper.*;

@SuppressWarnings("DefaultAnnotationParam")
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "IPSEnvelope", namespace = ROOT_URI_B08)
public class B08 extends PACSObject {
    public final static String MSG_DEF_IDR = "pacs.002.001.09";
    public static final String BIZ_SVC = "B08";

    @XmlElement(name = "AppHdr", namespace = HEADER_URI)
    private AppHdr header_AppHdr;
    @XmlElement(name = "Document", namespace = DOCUMENT_URI_B08)
    private Document document_Document;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AppHdr {
        @XmlElement(name = "Fr", namespace = HEADER_URI)
        private Fr fr;
        @XmlElement(name = "To", namespace = HEADER_URI)
        private To to;
        @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
        private String BizMsgIdr;
        @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
        private String MsgDefIdr;
        @XmlElement(name = "BizSvc", namespace = HEADER_URI)
        private String BizSvc;
        @XmlElement(name = "CreDt", namespace = HEADER_URI)
        private String CreDt;
        @XmlElement(name = "Sgntr", namespace = HEADER_URI)
        private Sgntr Sgntr;
        @XmlElement(name = "Rltd", namespace = HEADER_URI)
        private Rltd Rltd;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Fr {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class To {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIId {
            @XmlElement(name = "FinInstnId", namespace = HEADER_URI)
            private FinInstnId FinInstnId;

            @Data
            @AllArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            @NoArgsConstructor
            @XmlType(name="FinInstnId")
            public static class FinInstnId {
                @XmlElement(name = "Othr", namespace = HEADER_URI)
                private Othr Othr;

                @Data
                @AllArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                @NoArgsConstructor
                @XmlType(name="Othr")
                public static class Othr {
                    @XmlElement(name = "Id", namespace = HEADER_URI)
                    private String Id;
                }
            }
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Sgntr {
            @XmlElement(name = "Sign", namespace = SIGN_URI)
            private String sign;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Rltd {
            @XmlElement(name = "Fr", namespace = HEADER_URI)
            private Fr fr;
            @XmlElement(name = "To", namespace = HEADER_URI)
            private To to;
            @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
            private String BizMsgIdr;
            @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
            private String MsgDefIdr;
            @XmlElement(name = "BizSvc", namespace = HEADER_URI)
            private String BizSvc;
            @XmlElement(name = "CreDt", namespace = HEADER_URI)
            private String CreDt;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    @NoArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Document {
        @XmlElement(name = "FIToFIPmtStsRpt", namespace = DOCUMENT_URI_B08)
        private FIToFIPmtStsRpt FIToFIPmtStsRpt;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIToFIPmtStsRpt {
            @XmlElement(name = "GrpHdr", namespace = DOCUMENT_URI_B08)
            private GrpHdr GrpHdr;
            @XmlElement(name = "OrgnlGrpInfAndSts", namespace = DOCUMENT_URI_B08)
            private OrgnlGrpInfAndSts OrgnlGrpInfAndSts;
            @XmlElement(name = "TxInfAndSts", namespace = DOCUMENT_URI_B08)
            private TxInfAndSts TxInfAndSts;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class GrpHdr {
                @XmlElement(name = "MsgId", namespace = DOCUMENT_URI_B08)
                private String MsgId;
                @XmlElement(name = "CreDtTm", namespace = DOCUMENT_URI_B08)
                private String CreDtTm;
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class OrgnlGrpInfAndSts {
                @XmlElement(name = "OrgnlMsgId", namespace = DOCUMENT_URI_B08)
                private String OrgnlMsgId;
                @XmlElement(name = "OrgnlMsgNmId", namespace = DOCUMENT_URI_B08)
                private String OrgnlMsgNmId;
                @XmlElement(name = "OrgnlCreDtTm", namespace = DOCUMENT_URI_B08)
                private String OrgnlCreDtTm;
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class TxInfAndSts {
                @XmlElement(name = "OrgnlEndToEndId", namespace = DOCUMENT_URI_B08)
                private String OrgnlEndToEndId;
                @XmlElement(name = "OrgnlTxId", namespace = DOCUMENT_URI_B08)
                private String OrgnlTxId;
                @XmlElement(name = "StsRsnInf", namespace = DOCUMENT_URI_B08)
                private StsRsnInf StsRsnInf;
                @XmlElement(name = "AccptncDtTm", namespace = DOCUMENT_URI_B08)
                private String AccptncDtTm;
                @XmlElement(name = "OrgnlTxRef", namespace = DOCUMENT_URI_B08)
                private OrgnlTxRef OrgnlTxRef;
                @XmlElement(name = "RmtInf", namespace = DOCUMENT_URI_B08)
                private RmtInf RmtInf;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class StsRsnInf {
                    @XmlElement(name = "Orgtr", namespace = DOCUMENT_URI_B08)
                    private Orgtr Orgtr;
                    @XmlElement(name = "Rsn", namespace = DOCUMENT_URI_B08)
                    private Rsn Rsn;
                    @XmlElement(name = "AddtlInf", namespace = DOCUMENT_URI_B08)
                    private String AddtlInf;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Orgtr {
                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                        private Id id;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name="Id4")
                        public static class Id {
                            @XmlElement(name = "OrgId", namespace = DOCUMENT_URI_B08)
                            private OrgId OrgId;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            public static class OrgId {
                                @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                private Othr Othr;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name="Othr1")
                                public static class Othr {
                                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                    private String Id;
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Rsn {
                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                        private String Prtry;
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class OrgnlTxRef {
                    @XmlElement(name = "IntrBkSttlmAmt", namespace = DOCUMENT_URI_B08)
                    private IntrBkSttlmAmt IntrBkSttlmAmt;
                    @XmlElement(name = "Dbtr", namespace = DOCUMENT_URI_B08)
                    private Dbtr Dbtr;
                    @XmlElement(name = "DbtrAgt", namespace = DOCUMENT_URI_B08)
                    private DbtrAgt DbtrAgt;
                    @XmlElement(name = "CdtrAgt", namespace = DOCUMENT_URI_B08)
                    private CdtrAgt CdtrAgt;
                    @XmlElement(name = "Cdtr", namespace = DOCUMENT_URI_B08)
                    private Cdtr Cdtr;
                    @XmlElement(name = "CdtrAcct", namespace = DOCUMENT_URI_B08)
                    private CdtrAcct CdtrAcct;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class IntrBkSttlmAmt {
                        @XmlValue
                        private String IntrBkSttlmAmt;
                        @XmlAttribute(name = "Ccy")
                        private String Ccy;
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class DbtrAgt {
                        @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B08)
                        private FinInstnId FinInstnId;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="FinInstnId1")
                        public static class FinInstnId {
                            @XmlElement(name = "ClrSysMmbId", namespace = DOCUMENT_URI_B08)
                            private ClrSysMmbId ClrSysMmbId;
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name="ClrSysMmbId")
                            public static class ClrSysMmbId {
                                @XmlElement(name = "MmbId", namespace = DOCUMENT_URI_B08)
                                private String MmbId;
                            }

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name="Othr2")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class CdtrAcct {
                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                        private Id id;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name="Id1")
                        public static class Id {
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name="Othr5")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                                @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                private SchmeNm SchmeNm;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name="SchmeNm2")
                                public static class SchmeNm {
                                    @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                    private String Prtry;
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Dbtr {
                        @XmlElement(name = "Pty", namespace = DOCUMENT_URI_B08)
                        private Pty Pty;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name="Pty2")
                        public static class Pty {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                            private Id Id;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name="Id2")
                            public static class Id {
                                @XmlElement(name = "PrvtId", namespace = DOCUMENT_URI_B08)
                                private PrvtId PrvtId;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                public static class PrvtId {
                                    @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                    private Othr Othr;

                                    @Data
                                    @AllArgsConstructor
                                    @NoArgsConstructor
                                    @Builder
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name="Othr3")
                                    public static class Othr {
                                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                        private String Id;
                                        @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                        private SchmeNm SchmeNm;

                                        @Data
                                        @AllArgsConstructor
                                        @NoArgsConstructor
                                        @Builder
                                        @XmlAccessorType(XmlAccessType.FIELD)
                                        @XmlType(name="SchmeNm3")
                                        public static class SchmeNm {
                                            @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                            private String Prtry;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class CdtrAgt {
                        @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B08)
                        private FinInstnId FinInstnId;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class FinInstnId {
                            @XmlElement(name = "ClrSysMmbId", namespace = DOCUMENT_URI_B08)
                            private ClrSysMmbId ClrSysMmbId;
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name="ClrSysMmbId2")
                            public static class ClrSysMmbId {
                                @XmlElement(name = "MmbId", namespace = DOCUMENT_URI_B08)
                                private String MmbId;
                            }

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name="Othr4")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Cdtr {
                        @XmlElement(name = "Pty", namespace = DOCUMENT_URI_B08)
                        private Pty Pty;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name="Pty1")
                        public static class Pty {
                            @XmlElement(name = "Nm", namespace = DOCUMENT_URI_B08)
                            private String Nm;
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                            private Id Id;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name="Id3")
                            public static class Id {
                                @XmlElementWrapper(name = "OrgId", namespace = DOCUMENT_URI_B08)
                                @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                private List<Othr> OrgId;

                                @Data
                                @AllArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @NoArgsConstructor
                                @XmlType(name="Othr6")
                                public static class Othr {
                                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                    private String Id;
                                    @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                    private SchmeNm SchmeNm;

                                    @Data
                                    @AllArgsConstructor
                                    @Builder
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @NoArgsConstructor
                                    @XmlType(name="SchmeNm1")
                                    public static class SchmeNm {
                                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                        private String Prtry;
                                    }
                                }
                            }
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                @NoArgsConstructor
                public static class RmtInf {
                    @XmlElement(name = "Ustrd", namespace = DOCUMENT_URI_B08)
                    private String Ustrd;
                }
            }
        }
    }
}
