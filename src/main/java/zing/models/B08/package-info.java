@XmlSchema(
        namespace = ROOT_URI_B08,
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(namespaceURI = ROOT_URI_B08, prefix = ""),
                @XmlNs(namespaceURI = HEADER_URI, prefix = HEADER_PREFIX),
                @XmlNs(namespaceURI = DOCUMENT_URI_B08, prefix = DOCUMENT_PREFIX),
                @XmlNs(namespaceURI = SIGN_URI, prefix = SIGN_PREFIX),
                @XmlNs(namespaceURI = XSI_URI, prefix = XSI_PREFIX)
        }
)
package zing.models.B08;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

import static zing.models.B07.MyNamespacePrefixMapper.*;