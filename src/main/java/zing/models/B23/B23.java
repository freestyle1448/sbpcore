package zing.models.B23;

import lombok.*;
import zing.models.PACSObject;

import javax.xml.bind.annotation.*;
import java.util.List;

import static zing.models.B07.MyNamespacePrefixMapper.*;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "IPSEnvelope", namespace = ROOT_URI_B08)
public class B23 extends PACSObject {
    public final static String MSG_DEF_IDR = "pacs.002.001.09";
    public static final String BIZ_SVC = "B23";

    @XmlElement(name = "AppHdr", namespace = HEADER_URI)
    private AppHdr header_AppHdr;
    @XmlElement(name = "Document", namespace = DOCUMENT_URI_B08)
    private Document document_Document;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AppHdr {
        @XmlElement(name = "Fr", namespace = HEADER_URI)
        private AppHdr.Fr fr;
        @XmlElement(name = "To", namespace = HEADER_URI)
        private AppHdr.To to;
        @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
        private String BizMsgIdr;
        @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
        private String MsgDefIdr;
        @XmlElement(name = "BizSvc", namespace = HEADER_URI)
        private String BizSvc;
        @XmlElement(name = "CreDt", namespace = HEADER_URI)
        private String CreDt;
        @XmlElement(name = "Sgntr", namespace = HEADER_URI)
        private AppHdr.Sgntr Sgntr;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Fr {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private AppHdr.FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class To {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private AppHdr.FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIId {
            @XmlElement(name = "FinInstnId", namespace = HEADER_URI)
            private AppHdr.FIId.FinInstnId FinInstnId;

            @Data
            @AllArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            @NoArgsConstructor
            @XmlType(name = "FinInstnId")
            public static class FinInstnId {
                @XmlElement(name = "Othr", namespace = HEADER_URI)
                private AppHdr.FIId.FinInstnId.Othr Othr;

                @Data
                @AllArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                @NoArgsConstructor
                @XmlType(name = "Othr")
                public static class Othr {
                    @XmlElement(name = "Id", namespace = HEADER_URI)
                    private String Id;
                }
            }
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Sgntr {
            @XmlElement(name = "Sign", namespace = SIGN_URI)
            private String sign;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    @NoArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Document {
        @XmlElement(name = "FIToFIPmtStsRpt", namespace = DOCUMENT_URI_B08)
        private Document.FIToFIPmtStsRpt FIToFIPmtStsRpt;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIToFIPmtStsRpt {
            @XmlElement(name = "GrpHdr", namespace = DOCUMENT_URI_B08)
            private Document.FIToFIPmtStsRpt.GrpHdr GrpHdr;
            @XmlElement(name = "TxInfAndSts", namespace = DOCUMENT_URI_B08)
            private Document.FIToFIPmtStsRpt.TxInfAndSts TxInfAndSts;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class GrpHdr {
                @XmlElement(name = "MsgId", namespace = DOCUMENT_URI_B08)
                private String MsgId;
                @XmlElement(name = "CreDtTm", namespace = DOCUMENT_URI_B08)
                private String CreDtTm;
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class TxInfAndSts {
                @XmlElement(name = "OrgnlEndToEndId", namespace = DOCUMENT_URI_B08)
                private String OrgnlEndToEndId;
                @XmlElement(name = "OrgnlTxId", namespace = DOCUMENT_URI_B08)
                private String OrgnlTxId;
                @XmlElement(name = "TxSts", namespace = DOCUMENT_URI_B08)
                private String TxSts;
                @XmlElement(name = "StsRsnInf", namespace = DOCUMENT_URI_B08)
                private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf StsRsnInf;
                @XmlElement(name = "AccptncDtTm", namespace = DOCUMENT_URI_B08)
                private String AccptncDtTm;
                @XmlElement(name = "OrgnlTxRef", namespace = DOCUMENT_URI_B08)
                private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef OrgnlTxRef;
                @XmlElement(name = "SplmtryData", namespace = DOCUMENT_URI_B08)
                private SplmtryData SplmtryData;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class StsRsnInf {
                    @XmlElement(name = "Orgtr", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr Orgtr;
                    @XmlElement(name = "Rsn", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Rsn Rsn;
                    @XmlElement(name = "AddtlInf", namespace = DOCUMENT_URI_B08)
                    private String AddtlInf;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Orgtr {
                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id id;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "Id4")
                        public static class Id {
                            @XmlElement(name = "OrgId", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId OrgId;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            public static class OrgId {
                                @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.Othr Othr;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "Othr1")
                                public static class Othr {
                                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                    private String Id;
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Rsn {
                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                        private String Prtry;
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class OrgnlTxRef {
                    @XmlElement(name = "IntrBkSttlmAmt", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.IntrBkSttlmAmt IntrBkSttlmAmt;
                    @XmlElement(name = "IntrBkSttlmDt", namespace = DOCUMENT_URI_B08)
                    private String IntrBkSttlmDt;
                    @XmlElement(name = "RmtInf", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.RmtInf RmtInf;
                    @XmlElement(name = "Dbtr", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr Dbtr;
                    @XmlElement(name = "DbtrAgt", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt DbtrAgt;
                    @XmlElement(name = "CdtrAgt", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt CdtrAgt;
                    @XmlElement(name = "Cdtr", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr Cdtr;
                    @XmlElement(name = "CdtrAcct", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct CdtrAcct;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class IntrBkSttlmAmt {
                        @XmlValue
                        private String IntrBkSttlmAmt;
                        @XmlAttribute(name = "Ccy")
                        private String Ccy;
                    }

                    @Data
                    @AllArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @NoArgsConstructor
                    public static class RmtInf {
                        @XmlElement(name = "Ustrd", namespace = DOCUMENT_URI_B08)
                        private String Ustrd;
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class DbtrAgt {
                        @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.FinInstnId FinInstnId;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name = "FinInstnId1")
                        public static class FinInstnId {
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.FinInstnId.Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name = "Othr2")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class CdtrAcct {
                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id id;

                        @Data
                        @AllArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @NoArgsConstructor
                        @XmlType(name = "Id1")
                        public static class Id {
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id.Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @NoArgsConstructor
                            @XmlType(name = "Othr5")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                                @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id.Othr.SchmeNm SchmeNm;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "SchmeNm2")
                                public static class SchmeNm {
                                    @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                    private String Prtry;
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Dbtr {
                        @XmlElement(name = "Pty", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty Pty;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "Pty2")
                        public static class Pty {
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id Id;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "Id2")
                            public static class Id {
                                @XmlElement(name = "PrvtId", namespace = DOCUMENT_URI_B08)
                                private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId PrvtId;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                public static class PrvtId {
                                    @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId.Othr Othr;

                                    @Data
                                    @AllArgsConstructor
                                    @NoArgsConstructor
                                    @Builder
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name = "Othr3")
                                    public static class Othr {
                                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                        private String Id;
                                        @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId.Othr.SchmeNm SchmeNm;

                                        @Data
                                        @AllArgsConstructor
                                        @NoArgsConstructor
                                        @Builder
                                        @XmlAccessorType(XmlAccessType.FIELD)
                                        @XmlType(name = "SchmeNm3")
                                        public static class SchmeNm {
                                            @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                            private String Prtry;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class CdtrAgt {
                        @XmlElement(name = "FinInstnId", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId FinInstnId;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class FinInstnId {
                            @XmlElement(name = "ClrSysMmbId", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId.ClrSysMmbId ClrSysMmbId;
                            @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId.Othr Othr;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "ClrSysMmbId2")
                            public static class ClrSysMmbId {
                                @XmlElement(name = "MmbId", namespace = DOCUMENT_URI_B08)
                                private String MmbId;
                            }

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "Othr4")
                            public static class Othr {
                                @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                private String Id;
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Cdtr {
                        @XmlElement(name = "Pty", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty Pty;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "Pty1")
                        public static class Pty {
                            @XmlElement(name = "Nm", namespace = DOCUMENT_URI_B08)
                            private String Nm;
                            @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id Id;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "Id3")
                            public static class Id {
                                @XmlElementWrapper(name = "OrgId", namespace = DOCUMENT_URI_B08)
                                @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                private List<Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.Othr> OrgId;

                                @Data
                                @AllArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @NoArgsConstructor
                                @XmlType(name = "Othr6")
                                public static class Othr {
                                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                    private String Id;
                                    @XmlElement(name = "SchmeNm", namespace = DOCUMENT_URI_B08)
                                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.Othr.SchmeNm SchmeNm;

                                    @Data
                                    @AllArgsConstructor
                                    @Builder
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @NoArgsConstructor
                                    @XmlType(name = "SchmeNm1")
                                    public static class SchmeNm {
                                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                                        private String Prtry;
                                    }
                                }
                            }
                        }
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class SplmtryData {
                    @XmlElement(name = "Envlp", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.SplmtryData.Envlp Envlp;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Envlp {
                        @XmlElement(name = "IpsDt", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.SplmtryData.Envlp.IpsDt IpsDt;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class IpsDt {
                            @XmlElement(name = "FrScr", namespace = DOCUMENT_URI_B08)
                            private String FrScr;
                            @XmlElement(name = "OperDate", namespace = DOCUMENT_URI_B08)
                            private String OperDate;
                        }
                    }
                }
            }
        }
    }
}
