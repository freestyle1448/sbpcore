package zing.models.B24;

import lombok.*;
import zing.models.B07.B07;
import zing.models.PACSObject;

import javax.xml.bind.annotation.*;

import static zing.models.B07.MyNamespacePrefixMapper.*;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "IPSEnvelope", namespace = ROOT_URI_B08)
public class B24 extends PACSObject {
    public final static String MSG_DEF_IDR = "pacs.002.001.09";
    public static final String BIZ_SVC = "B24";

    @XmlElement(name = "AppHdr", namespace = HEADER_URI)
    private AppHdr header_AppHdr;
    @XmlElement(name = "Document", namespace = DOCUMENT_URI_B08)
    private Document document_Document;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AppHdr {
        @XmlElement(name = "Fr", namespace = HEADER_URI)
        private AppHdr.Fr fr;
        @XmlElement(name = "To", namespace = HEADER_URI)
        private AppHdr.To to;
        @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
        private String BizMsgIdr;
        @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
        private String MsgDefIdr;
        @XmlElement(name = "BizSvc", namespace = HEADER_URI)
        private String BizSvc;
        @XmlElement(name = "CreDt", namespace = HEADER_URI)
        private String CreDt;
        @XmlElement(name = "Sgntr", namespace = HEADER_URI)
        private AppHdr.Sgntr Sgntr;
        @XmlElement(name = "Rltd", namespace = HEADER_URI)
        private Rltd Rltd;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Fr {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private AppHdr.FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class To {
            @XmlElement(name = "FIId", namespace = HEADER_URI)
            private AppHdr.FIId FIId;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIId {
            @XmlElement(name = "FinInstnId", namespace = HEADER_URI)
            private AppHdr.FIId.FinInstnId FinInstnId;

            @Data
            @AllArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            @NoArgsConstructor
            @XmlType(name = "FinInstnId")
            public static class FinInstnId {
                @XmlElement(name = "Othr", namespace = HEADER_URI)
                private AppHdr.FIId.FinInstnId.Othr Othr;

                @Data
                @AllArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                @NoArgsConstructor
                @XmlType(name = "Othr")
                public static class Othr {
                    @XmlElement(name = "Id", namespace = HEADER_URI)
                    private String Id;
                }
            }
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Sgntr {
            @XmlElement(name = "Sign", namespace = SIGN_URI)
            private String sign;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Rltd {
            @XmlElement(name = "Fr", namespace = HEADER_URI)
            private AppHdr.Fr fr;
            @XmlElement(name = "To", namespace = HEADER_URI)
            private AppHdr.To to;
            @XmlElement(name = "BizMsgIdr", namespace = HEADER_URI)
            private String BizMsgIdr;
            @XmlElement(name = "MsgDefIdr", namespace = HEADER_URI)
            private String MsgDefIdr;
            @XmlElement(name = "BizSvc", namespace = HEADER_URI)
            private String BizSvc;
            @XmlElement(name = "CreDt", namespace = HEADER_URI)
            private String CreDt;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder
    @NoArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Document {
        @XmlElement(name = "FIToFIPmtStsRpt", namespace = DOCUMENT_URI_B08)
        private Document.FIToFIPmtStsRpt FIToFIPmtStsRpt;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class FIToFIPmtStsRpt {
            @XmlElement(name = "GrpHdr", namespace = DOCUMENT_URI_B08)
            private Document.FIToFIPmtStsRpt.GrpHdr GrpHdr;
            @XmlElement(name = "OrgnlGrpInfAndSts", namespace = DOCUMENT_URI_B08)
            private Document.FIToFIPmtStsRpt.OrgnlGrpInfAndSts OrgnlGrpInfAndSts;
            @XmlElement(name = "TxInfAndSts", namespace = DOCUMENT_URI_B08)
            private Document.FIToFIPmtStsRpt.TxInfAndSts TxInfAndSts;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class GrpHdr {
                @XmlElement(name = "MsgId", namespace = DOCUMENT_URI_B08)
                private String MsgId;
                @XmlElement(name = "CreDtTm", namespace = DOCUMENT_URI_B08)
                private String CreDtTm;
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class OrgnlGrpInfAndSts {
                @XmlElement(name = "OrgnlMsgId", namespace = DOCUMENT_URI_B08)
                private String OrgnlMsgId;
                @XmlElement(name = "OrgnlMsgNmId", namespace = DOCUMENT_URI_B08)
                private String OrgnlMsgNmId;
                @XmlElement(name = "OrgnlCreDtTm", namespace = DOCUMENT_URI_B08)
                private String OrgnlCreDtTm;
            }

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class TxInfAndSts {
                @XmlElement(name = "OrgnlEndToEndId", namespace = DOCUMENT_URI_B08)
                private String OrgnlEndToEndId;
                @XmlElement(name = "OrgnlTxId", namespace = DOCUMENT_URI_B08)
                private String OrgnlTxId;
                @XmlElement(name = "StsRsnInf", namespace = DOCUMENT_URI_B08)
                private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf StsRsnInf;
                @XmlElement(name = "AccptncDtTm", namespace = DOCUMENT_URI_B08)
                private String AccptncDtTm;
                @XmlElement(name = "OrgnlTxRef", namespace = DOCUMENT_URI_B08)
                private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef OrgnlTxRef;

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class StsRsnInf {
                    @XmlElement(name = "Orgtr", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr Orgtr;
                    @XmlElement(name = "Rsn", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Rsn Rsn;
                    @XmlElement(name = "AddtlInf", namespace = DOCUMENT_URI_B08)
                    private String AddtlInf;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Orgtr {
                        @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                        private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id id;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "Id4")
                        public static class Id {
                            @XmlElement(name = "OrgId", namespace = DOCUMENT_URI_B08)
                            private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId OrgId;

                            @Data
                            @AllArgsConstructor
                            @NoArgsConstructor
                            @Builder
                            @XmlAccessorType(XmlAccessType.FIELD)
                            public static class OrgId {
                                @XmlElement(name = "Othr", namespace = DOCUMENT_URI_B08)
                                private Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.Othr Othr;

                                @Data
                                @AllArgsConstructor
                                @NoArgsConstructor
                                @Builder
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "Othr1")
                                public static class Othr {
                                    @XmlElement(name = "Id", namespace = DOCUMENT_URI_B08)
                                    private String Id;
                                }
                            }
                        }
                    }

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Rsn {
                        @XmlElement(name = "Prtry", namespace = DOCUMENT_URI_B08)
                        private String Prtry;
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class OrgnlTxRef {
                    @XmlElement(name = "IntrBkSttlmAmt", namespace = DOCUMENT_URI_B08)
                    private Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.IntrBkSttlmAmt IntrBkSttlmAmt;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class IntrBkSttlmAmt {
                        @XmlValue
                        private String IntrBkSttlmAmt;
                        @XmlAttribute(name = "Ccy")
                        private String Ccy;
                    }
                }

                @Data
                @AllArgsConstructor
                @NoArgsConstructor
                @Builder
                @XmlAccessorType(XmlAccessType.FIELD)
                public static class SplmtryData {
                    @XmlElement(name = "Envlp", namespace = DOCUMENT_URI_B07)
                    private B07.Document.FIToFICstmrCdtTrf.CdtTrfTxInf.SplmtryData.Envlp Envlp;

                    @Data
                    @AllArgsConstructor
                    @NoArgsConstructor
                    @Builder
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class Envlp {
                        @XmlElement(name = "IpsDt", namespace = DOCUMENT_URI_B07)
                        private B07.Document.FIToFICstmrCdtTrf.CdtTrfTxInf.SplmtryData.Envlp.IpsDt IpsDt;

                        @Data
                        @AllArgsConstructor
                        @NoArgsConstructor
                        @Builder
                        @XmlAccessorType(XmlAccessType.FIELD)
                        public static class IpsDt {
                            @XmlElement(name = "FrScr", namespace = DOCUMENT_URI_B07)
                            private String FrScr;
                            @XmlElement(name = "OperDate", namespace = DOCUMENT_URI_B07)
                            private String OperDate;
                        }
                    }
                }
            }
        }
    }
}

