package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "banks")
public class Bank {
    @Id
    private Object id;
    private String name;
    private String identifier;
    private String BIN;
    private String BankID;
    private String IPSEndpoint;
    private Long OperationsIdentifierCounter;
    private Commission defaultCommission;
    private String note;
    private Boolean active;
}
