package zing.models.IPSTransaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import zing.models.B07.B07;
import zing.models.B08.B08;
import zing.models.B23.B23;
import zing.models.B24.B24;
import zing.models.Balance;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "ips_transactions")
public class IPSTransaction {
    @Id
    private ObjectId _id;
    @Field("OperationID")
    private String operationID;
    private Balance Amount;
    private String IPSID;
    private Sender Sender;
    private ObjectId Receiver;
    private ObjectId QRCode;
    private String Purpose;
    private String FraudID;
    private String OperationDate;
    private B07 ConfiramationRequestB07;
    private B08 ConfiramationResposneB08;
    private B23 AcknowledgementToBenificiaryBankB23;
    private B24 AcknowledgementResponseFromBenificiaryBankB24;
    private Integer Status;
    private Integer Stage;
    private String ErrorReason;
    private Date startDate;
    private Date endDate;
}
