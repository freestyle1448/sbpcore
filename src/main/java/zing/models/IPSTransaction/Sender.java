package zing.models.IPSTransaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Sender {
    private String accountID;
    private String bankBIN;
    private String bankID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sender sender = (Sender) o;
        return getAccountID().equals(sender.getAccountID()) &&
                getBankID().equals(sender.getBankID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccountID(), getBankBIN(), getBankID());
    }
}
