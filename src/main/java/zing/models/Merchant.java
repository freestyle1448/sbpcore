package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "merchants")
public class Merchant {
    @Id
    private ObjectId id;
    private String name;
    private String bank;
    private String identifier;
    private String LegalAccountNumber;
    private String IPSID;
    private String LegalName;
    private String MerchantName;
    private String TIDN;
    private Boolean active;
    private Balance operationBalance;
    private String note;
}
