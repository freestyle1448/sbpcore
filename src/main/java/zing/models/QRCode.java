package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "qrcodes")
public class QRCode {
    public static final String QR_STAT = "QRStat";
    public static final String QR_DYN = "QRDyn";

    @Id
    private ObjectId _id;
    private String name;
    private Balance amount;
    private String type;
    private ObjectId account;
    private Date creationDate;
    private String creationType;
    private Integer status;
    private String qrId;
    private String hash;
}
