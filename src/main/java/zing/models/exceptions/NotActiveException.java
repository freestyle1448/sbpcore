package zing.models.exceptions;

public class NotActiveException extends RuntimeException {
    public NotActiveException(String message) {
        super(message);
    }
}