package zing.models.psb.qr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QRCodePSB {
    private String QR_ID;
    private String QR_DATA;
    private String QR_IMG_REF;
    private String QR_PSB_ID;
}
