package zing.models.psb.qr;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QRResponse {
    @SerializedName("SBP_ID")
    private String SBP_ID;
    @SerializedName("SBP_ACCOUNT_NUMBER")
    private String SBP_ACCOUNT_NUMBER;
    @SerializedName("SBP_MERCHANT")
    private String SBP_MERCHANT;
}
