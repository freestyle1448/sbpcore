package zing.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import zing.models.Receiver;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "transactions")
public class Transaction {
    @Id
    private ObjectId id;
    private String senderType;
    private String senderId;
    private List<Receiver> receivers;
    private String purpose;
    private Boolean splitVisibleToReceivers;
    private Integer stage;
    private Integer status;
    private Date startDate;
    private Date endDate;
}
