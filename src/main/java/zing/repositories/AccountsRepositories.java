package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Account;

public interface AccountsRepositories extends MongoRepository<Account, ObjectId> {
}
