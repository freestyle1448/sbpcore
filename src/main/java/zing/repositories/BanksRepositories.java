package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Bank;

public interface BanksRepositories extends MongoRepository<Bank, ObjectId> {
    Bank findByIdentifier(String identifier);
}
