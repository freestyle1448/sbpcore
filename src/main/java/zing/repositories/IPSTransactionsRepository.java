package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.IPSTransaction.IPSTransaction;

public interface IPSTransactionsRepository extends MongoRepository<IPSTransaction, ObjectId> {
    IPSTransaction findByOperationID(String operationId);
}