package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import zing.models.Balance;
import zing.models.IPSTransaction.IPSTransaction;
import zing.models.Merchant;

import static zing.models.transaction.Status.IN_PROCESS;
import static zing.models.transaction.Status.SUCCESS;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public IPSTransaction findIPSTransactionByOperationId(String operationId) {
        Query query = new Query(Criteria.where("operationID").is(operationId)
                .and("Status").is(IN_PROCESS)
                .and("Stage").is(1));
        Update update = new Update();
        update.set("Status", SUCCESS);
        update.set("Stage", 2);

        return mongoTemplate.findAndModify(query, update, IPSTransaction.class);
    }

    public Merchant findAndModifyMerchantAdd(ObjectId merchantId, Balance balance) {
        Query findAndModify = new Query(Criteria
                .where("_id").is(merchantId)
                .and("operationBalance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("operationBalance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModify, update, Merchant.class);
    }

    public Merchant findAndModifyMerchantSub(ObjectId merchantId, Balance balance) {
        Query findAndModify = new Query(Criteria
                .where("_id").is(merchantId)
                .and("operationBalance.balance").gte(balance.getAmount())
                .and("operationBalance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("operationBalance.amount", -balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModify, update, Merchant.class);
    }
}
