package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Merchant;

public interface MerchantsRepositories extends MongoRepository<Merchant, ObjectId> {
}
