package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import zing.models.QRCode;

public interface QRCodesRepositories extends MongoRepository<QRCode, ObjectId> {
    QRCode findByQrId(String qrId);
}
