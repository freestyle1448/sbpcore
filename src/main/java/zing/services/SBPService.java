package zing.services;

import com.mongodb.MongoException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import zing.models.B07.B07;
import zing.models.B08.B08;
import zing.models.B23.B23;
import zing.models.B24.B24;

import java.util.concurrent.CompletableFuture;

public interface SBPService {
    @Retryable(
            value = {MongoException.class},
            maxAttempts = 100, backoff = @Backoff(50))
    CompletableFuture<B08> confirmationRequestFromIPS(B07 request);

    @Retryable(
            value = {MongoException.class},
            maxAttempts = 100, backoff = @Backoff(50))
    CompletableFuture<B24> acknowlegementToBenificiaryBank(B23 request);
}
