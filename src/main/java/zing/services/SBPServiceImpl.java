package zing.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zing.models.*;
import zing.models.B07.B07;
import zing.models.B08.B08;
import zing.models.B23.B23;
import zing.models.B24.B24;
import zing.models.IPSTransaction.IPSTransaction;
import zing.models.IPSTransaction.Sender;
import zing.models.exceptions.NotActiveException;
import zing.models.exceptions.NotAllowedException;
import zing.models.exceptions.NotFoundException;
import zing.repositories.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static zing.models.AnswerCode.RECEIVER;
import static zing.models.AnswerCode.SUCCESS_MES;
import static zing.models.QRCode.QR_DYN;
import static zing.models.QRCode.QR_STAT;
import static zing.models.transaction.Status.*;

@Service
@Transactional(noRollbackForClassName = {"NotFoundException", "NotAllowedException", "NotActiveException"})
public class SBPServiceImpl implements SBPService {
    private final IPSTransactionsRepository ipsTransactionsRepository;
    private final QRCodesRepositories qrCodesRepositories;
    private final AccountsRepositories accountsRepositories;
    private final MerchantsRepositories merchantsRepositories;
    private final BanksRepositories banksRepositories;
    private final ManualRepository manualRepository;

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MMDD hh:mm:ss.sssZ");
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMDD");

    public SBPServiceImpl(IPSTransactionsRepository ipsTransactionsRepository, QRCodesRepositories qrCodesRepositories, AccountsRepositories accountsRepositories, MerchantsRepositories merchantsRepositories, BanksRepositories banksRepositories, ManualRepository manualRepository) {
        this.ipsTransactionsRepository = ipsTransactionsRepository;
        this.qrCodesRepositories = qrCodesRepositories;
        this.accountsRepositories = accountsRepositories;
        this.merchantsRepositories = merchantsRepositories;
        this.banksRepositories = banksRepositories;
        this.manualRepository = manualRepository;
    }

    @Override
    public CompletableFuture<B08> confirmationRequestFromIPS(B07 request) {
        final List<B07.Document.FIToFICstmrCdtTrf.CdtTrfTxInf.Cdtr.Id.Othr> othrList = request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOrgId();
        final IPSTransaction ipsTransaction1 = ipsTransactionsRepository.findByOperationID(request.getHeader_AppHdr().getBizMsgIdr());

        if (ipsTransaction1 != null)
            if (ipsTransaction1.getConfiramationRequestB07() != null ||
                    ipsTransaction1.getConfiramationResposneB08() != null ||
                    ipsTransaction1.getAcknowledgementToBenificiaryBankB23() != null ||
                    ipsTransaction1.getAcknowledgementResponseFromBenificiaryBankB24() != null)
                throw new NotFoundException("Транзакция с указанными данными уже существует в базе!");

        String QRCodeId = null;
        for (B07.Document.FIToFICstmrCdtTrf.CdtTrfTxInf.Cdtr.Id.Othr othr : othrList) {
            if (othr.getSchmeNm().getPrtry().equals(QR_STAT) || othr.getSchmeNm().getPrtry().equals(QR_DYN))
                QRCodeId = othr.getId();
        }

        final QRCode qrCode = qrCodesRepositories.findByQrId(QRCodeId);
        if (qrCode == null)
            throw new NotFoundException("QRCode не найден!");

        final Optional<Account> accountOptional = accountsRepositories.findById(qrCode.getAccount());
        Account receiverAccount;
        if (accountOptional.isPresent()) {
            receiverAccount = accountOptional.get();
        } else
            throw new NotFoundException("Account не найден!");

        IPSTransaction ipsTransaction = IPSTransaction.builder()
                .operationID(request.getHeader_AppHdr().getBizMsgIdr())
                .Amount(Balance.builder()
                        .amount(Double.valueOf(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getIntrBkSttlmAmt().getIntrBkSttlmAmt()))
                        .currency(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getIntrBkSttlmAmt().getCcy())
                        .build())
                .IPSID(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId())
                .Sender(Sender.builder()
                        .accountID(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtr().getId().getPrvtId().getOthr().getId())
                        .bankBIN(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtrAgt().getFinInstnId().getClrSysMmbId().getMmbId())
                        .bankID(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtrAgt().getFinInstnId().getOthr().getId())
                        .build())
                .QRCode(qrCode.get_id())
                .Purpose(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getRmtInf().getUstrd())
                .FraudID(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getSplmtryData().getEnvlp().getIpsDt().getFrScr())
                .OperationDate(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getSplmtryData().getEnvlp().getIpsDt().getOperDate())
                .ConfiramationRequestB07(request)
                .Status(WAITING)
                .Stage(0)
                .startDate(new Date())
                .build();

        if (!qrCode.getAmount().equals(ipsTransaction.getAmount())) {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setEndDate(new Date());
            ipsTransaction.setErrorReason("Сумма в QR-коде не совпадает с суммой в запросе!");
            ipsTransactionsRepository.insert(ipsTransaction);

            throw new NotAllowedException("Сумма в QR-коде не совпадает с суммой в запросе!");
        }

        if (qrCode.getType().equals("dynamic") && !receiverAccount.getCreateDynamicQR_allowed()) {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setEndDate(new Date());
            ipsTransaction.setErrorReason("Владелец аккаунта не может создавать динамический QR-код");
            ipsTransactionsRepository.insert(ipsTransaction);

            throw new NotAllowedException("Владелец аккаунта не может создавать динамический QR-код");
        }

        if (qrCode.getType().equals("static") && !receiverAccount.getCreateStaticQR_allowed()) {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setEndDate(new Date());
            ipsTransaction.setErrorReason("Владелец аккаунта не может создавать статический QR-код");
            ipsTransactionsRepository.insert(ipsTransaction);

            throw new NotAllowedException("Владелец аккаунта не может создавать статический QR-код");
        }

        Merchant receiverMerchant;
        Bank receiverBank;
        if (receiverAccount.getActive()) {
            final Optional<Merchant> merchantOptional = merchantsRepositories.findById(receiverAccount.getMerchant());

            if (merchantOptional.isPresent()) {
                receiverMerchant = merchantOptional.get();
            } else {
                ipsTransaction.setStatus(DENIED);
                ipsTransaction.setErrorReason("Merchant не найден!");
                ipsTransaction.setEndDate(new Date());
                ipsTransactionsRepository.insert(ipsTransaction);

                throw new NotFoundException("Merchant не найден!");
            }

            receiverBank = banksRepositories.findByIdentifier(receiverMerchant.getBank());
            if (receiverBank == null) {
                ipsTransaction.setStatus(DENIED);
                ipsTransaction.setErrorReason("Bank не найден!");
                ipsTransaction.setEndDate(new Date());
                ipsTransactionsRepository.insert(ipsTransaction);

                throw new NotFoundException("Bank не найден!");
            }

            if (!receiverMerchant.getActive()) {
                ipsTransaction.setStatus(DENIED);
                ipsTransaction.setErrorReason("Merchant неактивен!");
                ipsTransaction.setEndDate(new Date());
                ipsTransactionsRepository.insert(ipsTransaction);

                throw new NotActiveException("Merchant неактивен!");
            }

            if (!receiverBank.getActive()) {
                ipsTransaction.setStatus(DENIED);
                ipsTransaction.setErrorReason("Bank неактивен!");
                ipsTransaction.setEndDate(new Date());
                ipsTransactionsRepository.insert(ipsTransaction);

                throw new NotActiveException("Bank неактивен!");
            }
        } else {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setErrorReason("Account неактивен!");
            ipsTransaction.setEndDate(new Date());
            ipsTransactionsRepository.insert(ipsTransaction);

            throw new NotActiveException("Account неактивен!");
        }

        ipsTransaction.setReceiver(receiverMerchant.getId());

        if (!request.getHeader_AppHdr().getTo().getFIId().getFinInstnId().getOthr().getId().equals(receiverBank.getBankID()) || //22
                !request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtrAcct().getId().getOthr().getId().equals(receiverMerchant.getLegalAccountNumber()) || //31
                request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOrgId().parallelStream().noneMatch(othr -> othr.getId().equals(receiverMerchant.getIPSID()) && othr.getSchmeNm().getPrtry().equals("MRCH")) || //139
                !request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getNm().equals(receiverMerchant.getLegalName()) || //131
                request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOrgId().parallelStream().noneMatch(othr -> othr.getId().equals(receiverMerchant.getMerchantName()) && othr.getSchmeNm().getPrtry().equals("MRNM")) || //140
                !request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOthr().getId().equals(receiverMerchant.getTIDN()) //97
        ) {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setErrorReason("Данные не совпадают!");
            ipsTransaction.setEndDate(new Date());
            ipsTransactionsRepository.insert(ipsTransaction);

            throw new NotFoundException("Данные не совпадают!");
        }

        final List<B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.Othr> orgids = new ArrayList<>();
        final List<B07.Document.FIToFICstmrCdtTrf.CdtTrfTxInf.Cdtr.Id.Othr> orgId = request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOrgId();
        orgId.add(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getId().getOthr());

        orgId.parallelStream().map(othr -> B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.Othr.builder()
                .Id(othr.getId())
                .SchmeNm(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.Othr.SchmeNm.builder()
                        .Prtry(othr.getSchmeNm().getPrtry())
                        .build())
                .build()).forEach(orgids::add);

        B08 b08 = B08.builder()
                .header_AppHdr(B08.AppHdr.builder()
                        .fr(B08.AppHdr.Fr.builder()
                                .FIId(B08.AppHdr.FIId.builder()
                                        .FinInstnId(B08.AppHdr.FIId.FinInstnId.builder()
                                                .Othr(B08.AppHdr.FIId.FinInstnId.Othr.builder()
                                                        .Id(request.getHeader_AppHdr().getTo().getFIId().getFinInstnId().getOthr().getId())
                                                        .build())
                                                .build())
                                        .build())
                                .build())
                        .to(B08.AppHdr.To.builder()
                                .FIId(B08.AppHdr.FIId.builder()
                                        .FinInstnId(B08.AppHdr.FIId.FinInstnId.builder()
                                                .Othr(B08.AppHdr.FIId.FinInstnId.Othr.builder()
                                                        .Id(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId())
                                                        .build())
                                                .build())
                                        .build())
                                .build())
                        .BizMsgIdr(request.getHeader_AppHdr().getBizMsgIdr())
                        .MsgDefIdr(B08.MSG_DEF_IDR)
                        .BizSvc(B08.BIZ_SVC)
                        .CreDt(dateFormatter.format(new Date()).replace(' ', 'T'))
                        .Sgntr(B08.AppHdr.Sgntr.builder()
                                .sign("")//TODO Формирование подписи
                                .build())
                        .Rltd(B08.AppHdr.Rltd.builder()
                                .fr(B08.AppHdr.Fr.builder()
                                        .FIId(B08.AppHdr.FIId.builder()
                                                .FinInstnId(B08.AppHdr.FIId.FinInstnId.builder()
                                                        .Othr(B08.AppHdr.FIId.FinInstnId.Othr.builder()
                                                                .Id(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId())
                                                                .build())
                                                        .build())
                                                .build())
                                        .build())
                                .to(B08.AppHdr.To.builder()
                                        .FIId(B08.AppHdr.FIId.builder()
                                                .FinInstnId(B08.AppHdr.FIId.FinInstnId.builder()
                                                        .Othr(B08.AppHdr.FIId.FinInstnId.Othr.builder()
                                                                .Id(request.getHeader_AppHdr().getTo().getFIId().getFinInstnId().getOthr().getId())
                                                                .build())
                                                        .build())
                                                .build())
                                        .build())
                                .BizMsgIdr(request.getHeader_AppHdr().getBizMsgIdr())
                                .MsgDefIdr(request.getHeader_AppHdr().getMsgDefIdr())
                                .BizSvc(request.getHeader_AppHdr().getBizSvc())
                                .CreDt(request.getHeader_AppHdr().getCreDt())
                                .build())
                        .build())
                .document_Document(B08.Document.builder()
                        .FIToFIPmtStsRpt(B08.Document.FIToFIPmtStsRpt.builder()
                                .GrpHdr(B08.Document.FIToFIPmtStsRpt.GrpHdr.builder()
                                        .MsgId(String.format("%s%s%s", simpleDateFormat.format(new Date()), receiverBank.getIPSEndpoint(), receiverBank.getOperationsIdentifierCounter().toString()))
                                        .CreDtTm(dateFormatter.format(new Date()).replace(' ', 'T'))
                                        .build())
                                .OrgnlGrpInfAndSts(B08.Document.FIToFIPmtStsRpt.OrgnlGrpInfAndSts.builder()
                                        .OrgnlMsgId(request.getDocument_Document().getFIToFICstmrCdtTrf().getGrpHdr().getMsgId())
                                        .OrgnlMsgNmId(request.getHeader_AppHdr().getBizSvc())
                                        .OrgnlCreDtTm(request.getDocument_Document().getFIToFICstmrCdtTrf().getGrpHdr().getCreDtTm())
                                        .build())
                                .TxInfAndSts(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.builder()
                                        .OrgnlEndToEndId(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getPmtId().getEndToEndId())
                                        .OrgnlTxId(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getPmtId().getTxId())
                                        .StsRsnInf(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.builder()
                                                .Orgtr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.builder()
                                                        .id(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.builder()
                                                                .OrgId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.builder()
                                                                        .Othr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.Othr.builder()
                                                                                .Id(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtrAgt().getFinInstnId().getOthr().getId())
                                                                                .build())
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .Rsn(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Rsn.builder()
                                                        .Prtry(RECEIVER + AnswerCode.SUCCESS + "000") //TODO 000 на VVV
                                                        .build())
                                                .AddtlInf(SUCCESS_MES)
                                                .build())
                                        .AccptncDtTm(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getAccptncDtTm())
                                        .OrgnlTxRef(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.builder()
                                                .IntrBkSttlmAmt(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.IntrBkSttlmAmt.builder()
                                                        .Ccy(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getIntrBkSttlmAmt().getCcy())
                                                        .IntrBkSttlmAmt(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getIntrBkSttlmAmt().getIntrBkSttlmAmt())
                                                        .build())
                                                .Dbtr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.builder()
                                                        .Pty(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.builder()
                                                                .Id(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.builder()
                                                                        .PrvtId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId.builder()
                                                                                .Othr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId.Othr.builder()
                                                                                        .Id(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtr().getId().getPrvtId().getOthr().getId())
                                                                                        .SchmeNm(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Dbtr.Pty.Id.PrvtId.Othr.SchmeNm.builder()
                                                                                                .Prtry(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtr().getId().getPrvtId().getOthr().getSchmeNm().getPrtry())
                                                                                                .build())
                                                                                        .build())
                                                                                .build())
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .DbtrAgt(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.builder()
                                                        .FinInstnId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.FinInstnId.builder()
                                                                .ClrSysMmbId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.FinInstnId.ClrSysMmbId.builder()
                                                                        .MmbId(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtrAgt().getFinInstnId().getClrSysMmbId().getMmbId())
                                                                        .build())
                                                                .Othr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.DbtrAgt.FinInstnId.Othr.builder()
                                                                        .Id(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getDbtrAgt().getFinInstnId().getOthr().getId())
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .CdtrAgt(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.builder()
                                                        .FinInstnId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId.builder()
                                                                .ClrSysMmbId(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId.ClrSysMmbId.builder()
                                                                        .MmbId(receiverBank.getBIN())
                                                                        .build())
                                                                .Othr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAgt.FinInstnId.Othr.builder()
                                                                        .Id(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtrAgt().getFinInstnId().getOthr().getId())
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .Cdtr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.builder()
                                                        .Pty(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.builder()
                                                                .Nm(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtr().getNm())
                                                                .Id(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.Cdtr.Pty.Id.builder()
                                                                        .OrgId(orgids)
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .CdtrAcct(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.builder()
                                                        .id(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id.builder()
                                                                .Othr(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id.Othr.builder()
                                                                        .Id(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtrAcct().getId().getOthr().getId())
                                                                        .SchmeNm(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.CdtrAcct.Id.Othr.SchmeNm.builder()
                                                                                .Prtry(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getCdtrAcct().getId().getOthr().getSchmeNm().getPrtry())
                                                                                .build())
                                                                        .build())
                                                                .build())
                                                        .build())
                                                .build())
                                        .RmtInf(B08.Document.FIToFIPmtStsRpt.TxInfAndSts.RmtInf.builder()
                                                .Ustrd(request.getDocument_Document().getFIToFICstmrCdtTrf().getCdtTrfTxInf().getRmtInf().getUstrd())
                                                .build())
                                        .build())
                                .build())
                        .build())
                .build();

        ipsTransaction.setConfiramationResposneB08(b08);
        ipsTransaction.setStatus(IN_PROCESS);
        ipsTransaction.setStage(1);

        receiverBank.setOperationsIdentifierCounter(receiverBank.getOperationsIdentifierCounter() + 1);
        banksRepositories.save(receiverBank);
        ipsTransactionsRepository.insert(ipsTransaction);
        return CompletableFuture.completedFuture(b08);
    }

    @Override
    public CompletableFuture<B24> acknowlegementToBenificiaryBank(B23 request) {
        final IPSTransaction ipsTransaction = manualRepository.findIPSTransactionByOperationId(request.getHeader_AppHdr().getBizMsgIdr());

        if (ipsTransaction != null) {
            if (ipsTransaction.getStatus() > 1 && ipsTransaction.getStage() > 1
                    && (ipsTransaction.getConfiramationRequestB07() != null ||
                    ipsTransaction.getConfiramationResposneB08() != null ||
                    ipsTransaction.getAcknowledgementToBenificiaryBankB23() != null ||
                    ipsTransaction.getAcknowledgementResponseFromBenificiaryBankB24() != null)) {
                ipsTransaction.setStage(1);
                ipsTransaction.setStatus(IN_PROCESS);
                ipsTransactionsRepository.save(ipsTransaction);

                throw new NotFoundException("Транзакция с указанными данными уже существует в базе!");
            }
        } else
            throw new NotFoundException("Транзакция с указанным OperationID несуществует в базе!");

        final Optional<Merchant> merchantOptional = merchantsRepositories.findById(ipsTransaction.getReceiver());
        Merchant receiverMerchant;

        if (merchantOptional.isPresent()) {
            receiverMerchant = merchantOptional.get();
        } else {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setEndDate(new Date());
            ipsTransaction.setErrorReason("Merchant не найден");
            ipsTransactionsRepository.save(ipsTransaction);

            throw new NotFoundException("Merchant не найден");
        }

        final Bank receiverBank = banksRepositories.findByIdentifier(receiverMerchant.getBank());

        ipsTransaction.setAcknowledgementToBenificiaryBankB23(request);

        //noinspection NewObjectEquality
        if (ipsTransaction.getAmount().getAmount().doubleValue() != Double.valueOf(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getIntrBkSttlmAmt().getIntrBkSttlmAmt()) || //44
                !ipsTransaction.getAmount().getCurrency().equals(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getIntrBkSttlmAmt().getCcy()) ||
                !ipsTransaction.getIPSID().equals(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId()) ||
                !ipsTransaction.getSender().equals(Sender.builder()
                        .accountID(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getDbtr().getPty().getId().getPrvtId().getOthr().getId())
                        .bankID(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getDbtrAgt().getFinInstnId().getOthr().getId())
                        .build()) ||
                !ipsTransaction.getPurpose().equals(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getRmtInf().getUstrd()) || //32
                !ipsTransaction.getFraudID().equals(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getSplmtryData().getEnvlp().getIpsDt().getFrScr()) || //109
                !ipsTransaction.getOperationDate().equals(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getSplmtryData().getEnvlp().getIpsDt().getOperDate())) { //112
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setErrorReason("Данные не совпадают!");
            ipsTransaction.setEndDate(new Date());
            ipsTransactionsRepository.save(ipsTransaction);

            throw new NotFoundException("Данные не совпадают!");
        }

        if (manualRepository.findAndModifyMerchantAdd(ipsTransaction.getReceiver(), ipsTransaction.getAmount()) != null) {
            B24 b24 = B24.builder()
                    .header_AppHdr(B24.AppHdr.builder()
                            .fr(B24.AppHdr.Fr.builder()
                                    .FIId(B24.AppHdr.FIId.builder()
                                            .FinInstnId(B24.AppHdr.FIId.FinInstnId.builder()
                                                    .Othr(B24.AppHdr.FIId.FinInstnId.Othr.builder()
                                                            .Id(request.getHeader_AppHdr().getTo().getFIId().getFinInstnId().getOthr().getId())
                                                            .build())
                                                    .build())
                                            .build())
                                    .build())
                            .to(B24.AppHdr.To.builder()
                                    .FIId(B24.AppHdr.FIId.builder()
                                            .FinInstnId(B24.AppHdr.FIId.FinInstnId.builder()
                                                    .Othr(B24.AppHdr.FIId.FinInstnId.Othr.builder()
                                                            .Id(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId())
                                                            .build())
                                                    .build())
                                            .build())
                                    .build())
                            .BizMsgIdr(request.getHeader_AppHdr().getBizMsgIdr())
                            .MsgDefIdr(B24.MSG_DEF_IDR)
                            .BizSvc(B24.BIZ_SVC)
                            .CreDt(dateFormatter.format(new Date()).replace(' ', 'T'))
                            .Sgntr(B24.AppHdr.Sgntr.builder()
                                    .sign("") //TODO формирование подписи
                                    .build())
                            .Rltd(B24.AppHdr.Rltd.builder()
                                    .fr(B24.AppHdr.Fr.builder()
                                            .FIId(B24.AppHdr.FIId.builder()
                                                    .FinInstnId(B24.AppHdr.FIId.FinInstnId.builder()
                                                            .Othr(B24.AppHdr.FIId.FinInstnId.Othr.builder()
                                                                    .Id(request.getHeader_AppHdr().getFr().getFIId().getFinInstnId().getOthr().getId())
                                                                    .build())
                                                            .build())
                                                    .build())
                                            .build())
                                    .to(B24.AppHdr.To.builder()
                                            .FIId(B24.AppHdr.FIId.builder()
                                                    .FinInstnId(B24.AppHdr.FIId.FinInstnId.builder()
                                                            .Othr(B24.AppHdr.FIId.FinInstnId.Othr.builder()
                                                                    .Id(request.getHeader_AppHdr().getTo().getFIId().getFinInstnId().getOthr().getId())
                                                                    .build())
                                                            .build())
                                                    .build())
                                            .build())
                                    .BizMsgIdr(request.getHeader_AppHdr().getBizMsgIdr())
                                    .MsgDefIdr(request.getHeader_AppHdr().getMsgDefIdr())
                                    .BizSvc(request.getHeader_AppHdr().getBizSvc())
                                    .CreDt(request.getHeader_AppHdr().getCreDt())
                                    .build())
                            .build())
                    .document_Document(B24.Document.builder()
                            .FIToFIPmtStsRpt(B24.Document.FIToFIPmtStsRpt.builder()
                                    .GrpHdr(B24.Document.FIToFIPmtStsRpt.GrpHdr.builder()
                                            .MsgId(String.format("%s%s%s", simpleDateFormat.format(new Date()), receiverBank.getIPSEndpoint(), receiverBank.getOperationsIdentifierCounter().toString()))
                                            .CreDtTm(dateFormatter.format(new Date()).replace(' ', 'T'))
                                            .build())
                                    .OrgnlGrpInfAndSts(B24.Document.FIToFIPmtStsRpt.OrgnlGrpInfAndSts.builder()
                                            .OrgnlMsgId(request.getDocument_Document().getFIToFIPmtStsRpt().getGrpHdr().getMsgId())
                                            .OrgnlMsgNmId(request.getHeader_AppHdr().getBizSvc())
                                            .OrgnlCreDtTm(request.getDocument_Document().getFIToFIPmtStsRpt().getGrpHdr().getCreDtTm())
                                            .build())
                                    .TxInfAndSts(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.builder()
                                            .OrgnlEndToEndId(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlEndToEndId())
                                            .OrgnlTxId(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxId())
                                            .StsRsnInf(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.builder()
                                                    .Orgtr(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.builder()
                                                            .id(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.builder()
                                                                    .OrgId(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.builder()
                                                                            .Othr(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Orgtr.Id.OrgId.Othr.builder()
                                                                                    .Id(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getCdtrAgt().getFinInstnId().getOthr().getId())
                                                                                    .build())
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .Rsn(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.StsRsnInf.Rsn.builder()
                                                            .Prtry(RECEIVER + AnswerCode.SUCCESS + "000") //TODO 000 на VVV
                                                            .build())
                                                    .AddtlInf(SUCCESS_MES)
                                                    .build())
                                            .AccptncDtTm(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getAccptncDtTm())
                                            .OrgnlTxRef(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.builder()
                                                    .IntrBkSttlmAmt(B24.Document.FIToFIPmtStsRpt.TxInfAndSts.OrgnlTxRef.IntrBkSttlmAmt.builder()
                                                            .IntrBkSttlmAmt(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getIntrBkSttlmAmt().getIntrBkSttlmAmt())
                                                            .Ccy(request.getDocument_Document().getFIToFIPmtStsRpt().getTxInfAndSts().getOrgnlTxRef().getIntrBkSttlmAmt().getCcy())
                                                            .build())
                                                    .build())
                                            .build())
                                    .build())
                            .build())
                    .build();

            receiverBank.setOperationsIdentifierCounter(receiverBank.getOperationsIdentifierCounter() + 1);
            banksRepositories.save(receiverBank);

            ipsTransaction.setStatus(SUCCESS);
            ipsTransaction.setStage(2);
            ipsTransaction.setAcknowledgementResponseFromBenificiaryBankB24(b24);
            ipsTransaction.setEndDate(new Date());
            ipsTransactionsRepository.save(ipsTransaction);
            return CompletableFuture.completedFuture(b24);
        } else {
            ipsTransaction.setStatus(DENIED);
            ipsTransaction.setEndDate(new Date());
            ipsTransaction.setErrorReason("Merchant с указанным ID или валютой не найден!");
            ipsTransactionsRepository.save(ipsTransaction);

            throw new NotFoundException("Merchant с указанным ID или валютой не найден!");
        }
    }
}
