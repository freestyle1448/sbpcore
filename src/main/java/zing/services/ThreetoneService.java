package zing.services;

import org.springframework.scheduling.annotation.Async;
import zing.models.psb.qr.QRCodePSB;
import zing.models.psb.qr.QRStatus;

import java.util.concurrent.CompletableFuture;

@Async(value = "threadPoolTaskExecutor")
public interface ThreetoneService {
    CompletableFuture<QRCodePSB> genQR(String amount, String purpose, String id);

    CompletableFuture<QRStatus> getStatus(String qr_id);
}
