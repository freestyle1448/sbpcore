package zing.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zing.app.PSBHTTPRequests;
import zing.models.psb.input.InputRequest;
import zing.models.psb.qr.QRCodePSB;
import zing.models.psb.qr.QRRequest;
import zing.models.psb.qr.QRStatus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;

@Service
public class ThreetoneServiceImpl implements ThreetoneService {
    private final RestTemplate restTemplate;
    private final PSBHTTPRequests psbhttpRequests;
    private final SimpleDateFormat timestamp = new SimpleDateFormat("yMMddHHmmss");
    private Random rnd = new Random(System.currentTimeMillis());

    {
        timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public ThreetoneServiceImpl(RestTemplate restTemplate, PSBHTTPRequests psbhttpRequests) {
        this.restTemplate = restTemplate;
        this.psbhttpRequests = psbhttpRequests;
    }

    @Override
    public CompletableFuture<QRCodePSB> genQR(String amount, String purpose, String id) {
        //CompletableFuture<QRCodePSB> qrFuture = new CompletableFuture<>();
        InputRequest inputRequest = InputRequest.builder()
                .AMOUNT(amount)
                .ORDER(id != null && !id.isEmpty() ? id : String.valueOf(System.currentTimeMillis()))
                .DESC(purpose != null ? purpose : "pay")
                .TERMINAL("29447201")
                .MERCH_NAME("ZINGTOGETHER")
                .MERCHANT("899929447201")
                .EMAIL("freestyle1448@gmail.com")
                .TIMESTAMP(timestamp.format(new Date()))
                .NONCE(Long.toHexString(rnd.nextLong()))
                .build();
        //inputRequest.createSign();

        QRRequest qrRequest = new QRRequest(inputRequest);
        qrRequest.setSBP_ID("LA0000000012");
        qrRequest.setSBP_ACCOUNT_NUMBER("40702810700000216812");
        qrRequest.setSBP_MERCHANT("MA0000000014");
        qrRequest.setREGIME("mobile");
        qrRequest.createSign();
       /* psbhttpRequests.genQR(qrRequest,"https://3ds.payment.ru/cgi-bin/SBP/reg_qr")
                .whenComplete((qrCodePSB, throwable1) -> qrFuture.complete(qrCodePSB));*/
        /*psbhttpRequests.getTerminalInfo(inputRequest, "https://3ds.payment.ru/cgi-bin/SBP/get_terminal_parms")
                .whenComplete((qrResponse, throwable) -> {
                    if (qrResponse != null) {
                        QRRequest qrRequest = new QRRequest(inputRequest);
                        qrRequest.setSBP_ID(qrResponse.getSBP_ID());
                        qrRequest.setSBP_ACCOUNT_NUMBER(qrResponse.getSBP_ACCOUNT_NUMBER());
                        qrRequest.setSBP_MERCHANT(qrResponse.getSBP_MERCHANT());
                        qrRequest.setREGIME("mobile");
                        qrRequest.createSign();
                        psbhttpRequests.genQR(qrRequest,"https://3ds.payment.ru/cgi-bin/SBP/reg_qr")
                        .whenComplete((qrCodePSB, throwable1) -> qrFuture.complete(qrCodePSB));
                    }
                });*/
        return psbhttpRequests.genQR(qrRequest, "https://3ds.payment.ru/cgi-bin/SBP/reg_qr");
    }

    @Override
    public CompletableFuture<QRStatus> getStatus(String qr_id) {
        return psbhttpRequests.getQRStatus(qr_id, "https://3ds.payment.ru/cgi-bin/SBP/get_qr_status");
    }
}
